/*
2136838
Alexander Roxas
*/
package application;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bikes = new Bicycle[4];
        bikes[0] = new Bicycle("Apple", 21, 40.0);
        bikes[1] = new Bicycle("SundayMorning", 12, 50.0);
        bikes[2] = new Bicycle("RainsIs", 32, 53.0);
        bikes[3] = new Bicycle("FallingDown", 6, 20);

        for (int i = 0; i < bikes.length; i++){
            System.out.println(bikes[i]);
        }
    }
}
